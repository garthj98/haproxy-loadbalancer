# Settign up a load balancer 

A loadbalancer is a way of splitting traffic across multiple serves so that one does not get overloaded. 

This script allows one loadbalancer and 2 servers

### Prerequisits 
- A bit bucket repo
- 3 ubuntu machines
- use `git clone git@bitbucket.org:garthj98/haproxy-loadbalancer.git`
- have downloaded both (all saved in the same folder): 
  - `bash_install_nginx.sh`
  - `ha-proxy.sh`
  
# Steps 
- Run `$ ./ha-proxy.sh $1 $2 $3` in the format
  - Where $1 = the public ip address of the loadbalancer
  - $2 = public IP of server 1 
  - $3 = public IP of server 2
  - It will then ask for the location of the Secure key

## Steps within the code
- Start a new bitbucket repo to keep code 
- Make 3 ubuntu machines 
- Set up nginx on 2 of the machines 
- Set up a HA proxy on one of the machines 
- Configure HA proxy to load balance 