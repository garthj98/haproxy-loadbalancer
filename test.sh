# Ensure all IPs are entered
if [ -z "$3" ]
then 
    echo "You forgot at least 1 IP address, please try again."
    exit 1
fi

IPADDRESS=$1

# make script second argument be key name 
KEYNAME=garth-key-AWS.pem
if [ -z "$KEYNAME" ]
then 
    read -p "Enter Key name: " KEYNAME
fi
KEYLOC=~/.ssh/$KEYNAME

# make script third argument become user
USER=ubuntu

# Install and set up nginx on the two servers
SERVERNO=1
for server in $2 $3
do
    #./bash_install_nginx.sh $server $SERVERNO
    echo PIP$SERVERNO
    ((SERVERNO++))
done

PIP1=$(ssh -o StrictHostKeyChecking=no -i $KEYLOC $USER@$2 "hostname -I" 2>&1)
PIP2=$(ssh -o StrictHostKeyChecking=no -i $KEYLOC $USER@$3 "hostname -I" 2>&1)

echo $PIP1
echo $PIP2
