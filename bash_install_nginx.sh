# Create a bash script that takes in arguments and does the installation

#read -p "Enter Username (ubuntu or ec2-user): " USER
# sets login username
USER=ubuntu

# sets key location
KEYLOC=$3

# Sets public IP for server
IPADDRESS=$1
if [ -z "$IPADDRESS" ]
then 
    read -p "You forgot to enter the IP, please enter: " IPADDRESS
fi

# Set which server it is in the loadbalancer 
SERVERNO=$2
# what happens if i dont give a host name (hostname)
# create an if condiition that checks that script has been called with argument
# if it has, set it to hostnam, else let the user know to call the script with a ip  -o StrickHostKeyChecking=no

# Sends child script to virtual machine 
ssh -o StrictHostKeyChecking=no -i $KEYLOC $USER@$IPADDRESS << EOF
sudo apt update

if sudo nginx -v ; 
then
    echo 'nginx is already installed'
else
    sudo apt install nginx -y
fi

# Create an if condition when you check if nginx already installed. 
# if installed do nothing 
# else install 

sudo systemctl start nginx 

sudo sh -c "cat >/var/www/html/index.html <<_END_
<h1>
This is server $SERVERNO.
</h1>
<p>
The changing server number is from a load balancer
</p>
_END_"
EOF
##open http://$IPADDRESS
