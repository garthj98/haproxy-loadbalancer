# Get input from arguments of IP to use in the script
# First argument passed to the script should be IP 

# Ensure all IPs are entered
if [ -z "$3" ]
then 
    echo "You forgot at least 1 IP address, please try again."
    exit 1
fi

IPADDRESS=$1

# make script second argument be key name 
if [ -z "$KEYNAME" ]
then 
    read -p "Enter Key name(eg garth-key-AWS.pem): " KEYNAME
fi
KEYLOC=~/.ssh/$KEYNAME

# make script third argument become user
USER=ubuntu

# Install and set up nginx on the two servers
SERVERNO=1
for server in $2 $3
do
    ./bash_install_nginx.sh $server $SERVERNO $KEYLOC
    ((SERVERNO++))
done

# Set private IP of both servers
PIP1=$(ssh -o StrictHostKeyChecking=no -i $KEYLOC $USER@$2 "hostname -I" 2>&1)
PIP2=$(ssh -o StrictHostKeyChecking=no -i $KEYLOC $USER@$3 "hostname -I" 2>&1)
#PIP1=$4
#PIP2=$5

# use SSH + keys + user + IP to send shell commands to install and configure HA proxy 
ssh -o StrictHostKeyChecking=no -i $KEYLOC $USER@$IPADDRESS << EOF
# Update the ubuntu server 
sudo apt update

# Install haproxy 
sudo apt install haproxy -y

# Edit haproxy configuration file 
sudo sh -c "cat >> /etc/haproxy/haproxy.cfg <<_END_ 

frontend http_front
    bind *:80
    stats uri /haproxy?stats
    default_backend http_back

backend http_back
    balance roundrobin
    server server1 ${PIP1// /}:80 check
    server server2 ${PIP2// /}:80 check
_END_"

sudo systemctl restart haproxy

EOF
open http://$IPADDRESS

